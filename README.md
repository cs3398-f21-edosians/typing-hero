![logo](./logo.png)

## Team
* Collan Parker
* Lucas Grogean
* Peter Nguyen
* Valentin Jaimes-Costilla
* Nathaniel Huish

## Status of Project
### Features:
* Real time stats - complete
* Always Updating Dictionaries - complete
* Custom Dictionaries - complete
* Various Metrics - complete
* Arcade Mode - complete
* Hard Mode - complete
* Database - complete

## Sprint 3 Review
### Demo branch is 'main'
### Team members' next steps
* Collan Parker - Implementing the "forgot password" button.
* Lucas Grogean - Assisted Collan with the backend, did refactoring, and create a display for the User and Hi-Score
* Peter Nguyen - Changed Main Menu button to a Donation button for better usage of the website
* Valentin Jaimes-Costilla - Include a small window with a list of all hotkeys that the user has the option to open/close.
* Nathaniel Huish - Refine added features to behave more consistently, look cleaner

### Indivdual reviews
* **Nathaniel Huish**
Added feature: monetization visualization possibilities
Feature branch: [feature/monetization](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/034abca69eac1f2d2c74b3a83b1bd98dc1856f01/?at=feature%2Fmonetization)

| File         |  Purpose |
|:--------------:|:-----------:|
| [money.png](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/034abca69eac1f2d2c74b3a83b1bd98dc1856f01/money.png?at=feature%2Fmonetization) | Image placeholder for ad at top of game page that will make us a ton of money |
| [index.html](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/034abca69eac1f2d2c74b3a83b1bd98dc1856f01/index.html?at=feature%2Fmonetization) | Modified to add ad placeholder to the top of the game page |

Worked on feature: added minigame to hard-mode
Feature branch: [feature/Hard-Mode](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/874eba5e1eb6cb571863a622167d20a2d1a0e6c5/?at=feature%2FHard-Mode)

| File         |  Purpose |
|:--------------:|:-----------:|
| [snake.html](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/89d862df77cdc6020a2ce55d14b9bd22289156fa/snake.html?at=feature%2FHard-Mode) | Added snake minigame that will popup when user is in hard mode and misses a word |
| [main.js](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/89d862df77cdc6020a2ce55d14b9bd22289156fa/main.js?at=feature%2FHard-Mode) | Added logic to main game logic to open snake game in another window when the user misses a word |

* **Lucas Grogean**
(https://bitbucket.org/cs3398-f21-edosians/typing-hero/commits/e26a3b470e78515308b6255df4e203b0571fe444)
This commit does work for displaying the user data on the main page
(https://bitbucket.org/cs3398-f21-edosians/typing-hero/commits/b284c58617f1286e57e9311519985cdd3d0e9344)
This commit finishes up work for the user display and hi-score display, as well as refactors large portions of the signIn.html file
(https://bitbucket.org/cs3398-f21-edosians/typing-hero/commits/da8d35cba760e0063eeebc3afdf34bbac5de4b84)
This commit does a small bug change for text alignment on the login page
* **Collan Parker**
[signIn.html](https://bitbucket.org/cs3398-f21-edosians/typing-hero/commits/e5d0d31a3ee78642b5b25813e3b1e13d55f62349)
This commit just implements the logic behind logging in and registering.
The signIn.html page can now also read/write data to/from a google sheet.
* **Peter Nguyen**
(https://bitbucket.org/cs3398-f21-edosians/typing-hero/commits/aa8d6767de0459a733e295a49d28cb6e025e8cd5)
This commit replaced the Main Menu button with Donation and linked in the username for Venmo/Cashapp.
* **Valentin Jaimes-Costilla**
[main.js](https://bitbucket.org/cs3398-f21-edosians/typing-hero/commits/64a4781008a839d7de5bbef5ac046f11ee6e763f)
Commit for merge conflict fix. Does not show all the conflicting code for some reason but the merge conflict was much more than what is shown in the link above.
[index.html](https://bitbucket.org/cs3398-f21-edosians/typing-hero/commits/89d862df77cdc6020a2ce55d14b9bd22289156fa)
commit shows code for hotkey functionallity. currently only works on windows. Script is written on the head section of the index.html file so as to affect the entire page.
when Alt + r is pressed, the text and counter are reset.

## Sprint 2 Review
### Demo branch is 'main'
### Team members' next steps
* Collan Parker - Create a front end for logging in and registering
* Lucas Grogean - Fill in here
* Peter Nguyen - Fully implement the Donation button and have the TXST Song working.
* Valentin Jaimes-Costilla - created function to extract current text from text box and reverse the strings. 
* Nathaniel Huish - improve ability to add code without turning the existing code into a mess. This will be apparent if existing files don't become a mess.

### Indivdual reviews
* **Nathaniel Huish**
Added feature: user custom dictionaries
Feature branch: [feature/dictionaries](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/0dfa3a2e3427a92b426c4395eaf78e207f745af3/?at=feature%2Fdictionaries)

| File         |  Purpose |
|:--------------:|:-----------:|
| [main.js](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/0dfa3a2e3427a92b426c4395eaf78e207f745af3/main.js?at=feature%2Fdictionaries) | Contains the logic for showing/hiding user input box for custom text, implementing user text in game |
| [index.html](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/0dfa3a2e3427a92b426c4395eaf78e207f745af3/index.html?at=feature%2Fdictionaries) | Modified to add button to enter text input screen |
| [play-button.png](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/0dfa3a2e3427a92b426c4395eaf78e207f745af3/play-button.png?at=feature%2Fdictionaries) | This image gets displayed as the button for the user to click to play with their custom input |

Worked on feature: hard mode
Feature branch: [feature/Hard-Mode](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/874eba5e1eb6cb571863a622167d20a2d1a0e6c5/?at=feature%2FHard-Mode)

| File         |  Purpose |
|:--------------:|:-----------:|
| [main.js](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/874eba5e1eb6cb571863a622167d20a2d1a0e6c5/main.js?at=feature%2FHard-Mode) | Added hard mode function that gets called when hard mode button is clicked, this function will point to all other hard mode functions |
| [index.html](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/874eba5e1eb6cb571863a622167d20a2d1a0e6c5/index.html?at=feature%2FHard-Mode) | Added hard mode button to display |
| [hard-mode-button.png](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/874eba5e1eb6cb571863a622167d20a2d1a0e6c5/hard-mode-button.png?at=feature%2FHard-Mode) | This image gets displayed as the button for the user to click to enable hard mode |

* **Lucas Grogean**
Fill in here
* **Collan Parker**
Created the front end for user log-in/registration
[signIn.html](https://bitbucket.org/cs3398-f21-edosians/typing-hero/commits/168228b0eb8be8e28ec8c09615a61685f6580f98)
* **Peter Nguyen**
Added a donation menu button with donation link: https://www.w3schools.com/howto/howto_js_dropdown.asp
Added a button for the TXST Alma Mater
* **Valentin Jaimes-Costilla**
Worked on feature: hard mode
Feature branch: [feature/Hard-Mode](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/874eba5e1eb6cb571863a622167d20a2d1a0e6c5/?at=feature%2FHard-Mode)

| File         |  Purpose |
|:--------------:|:-----------:|
| [main.js](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/0dfa3a2e3427a92b426c4395eaf78e207f745af3/main.js?at=feature%2Fdictionaries) | Contains function that reverses current text at bottom of file. Code is commented with my name for easier detection |
| [index.html](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/0dfa3a2e3427a92b426c4395eaf78e207f745af3/index.html?at=feature%2Fdictionaries) | Contains section of code that displays the hard mode button which would run 'setReverse' when pushed |
| [hard-mode-button.png](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/874eba5e1eb6cb571863a622167d20a2d1a0e6c5/hard-mode-button.png?at=feature%2FHard-Mode) | This image gets displayed as the button for the user to click to enable hard mode |


## Sprint 1 Review
### Demo branch is 'main'
* **Lucas Grogean**  
Lucas Grogean spent several hours of his time learning more about the base project, and how html interacts with css and javascript. This research was done with the intent of understanding how to edit the software in the future.
* **Nathaniel Huish**  
Added feature: Texas State University CSS site theme.  
Feature branch: [feature/add-new-themes](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/55e0b935849b5cfc676c1ae945b92be33cc75e8d/?at=feature%2Fadd-new-themes)

| File         |  Purpose |
|:--------------:|:-----------:|
| [themes/txst.css](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/55e0b935849b5cfc676c1ae945b92be33cc75e8d/themes/txst.css?at=feature%2Fadd-new-themes) | Contains the color definitions for the page elements. Adds some school pride to the project |
| [logo-white.png](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/55e0b935849b5cfc676c1ae945b92be33cc75e8d/logo-white.png?at=feature%2Fadd-new-themes)      | This is the logo that gets populated into the page at load time. Adds some team pride to the project |
| [themes/theme-list.json](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/55e0b935849b5cfc676c1ae945b92be33cc75e8d/themes/theme-list.json?at=feature%2Fadd-new-themes) | This had to be modified for my theme to show up in the theme picking screen. Makes picking the Texas State Theme a smoother experience |
| [index.html](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/55e0b935849b5cfc676c1ae945b92be33cc75e8d/index.html?at=feature%2Fadd-new-themes) | This had to be modified to load the new title image |
| [style.css](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/55e0b935849b5cfc676c1ae945b92be33cc75e8d/style.css?at=feature%2Fadd-new-themes) | Removed fade that was getting stuck |

Added feature: "Real Time" WPM calculation which refreshes every second.  
Feature branch: [feature/real-time-wpm](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/d16c8d99e0c8dc46669925cc53911e521d0bbbfa/?at=feature%2Freal-time-wpm)

| File         |  Purpose |
|:--------------:|:-----------:|
| [index.html](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/d16c8d99e0c8dc46669925cc53911e521d0bbbfa/index.html?at=feature%2Freal-time-wpm) | Added WPM mode button which calls realTimeWPM() javascript method. Button is implemented with existing HTML checkbox so that the feature can be toggled by the user |
| [main.js](https://bitbucket.org/cs3398-f21-edosians/typing-hero/src/d16c8d99e0c8dc46669925cc53911e521d0bbbfa/main.js?at=feature%2Freal-time-wpm) | Added realTimeWPM() method so that WPM display on page will be updated every second. This is more of a rolling-average implementation that gives the user an idea of their current performance. This function ties the existing showResult() function to a js timer so that the function is called every second, regardless of everything else that is going on during the game |

* **Collan Parker**  
Spent the majority of the sprint familiarizing with javascript, css, and html.
Javascript Tutorials: https://www.w3schools.com/js/
                      https://www.youtube.com/watch?v=W6NZfCO5SIk

Also spent a few hours learning how to use git's basic features 
(fork/merge branches, stage, commit, push, and pull).

Was able to add a theme and a library. The theme can be used by selecting 
"fiesta" in the "themes" tab. The theme was added because it adds another level 
of user customizability. 
https://bitbucket.org/cs3398-f21-edosians/typing-hero/commits/f4750ca138686ba61489e84990be52022b369c1c
https://bitbucket.org/cs3398-f21-edosians/typing-hero/commits/74464890169a28b1d80f8e1b362edea059541691

* **Peter Nguyen**  
Spent a few hours to research and understand Javascript, HTML, CSS, and Git. After researching, I have a better understanding on how the project works and what can be added to it. 
Javascript Tutorial: https://www.w3schools.com/js/
HTML Tutorial: https://www.w3schools.com/html/
CSS: Tutorial: https://www.w3schools.com/css/
Git Tutorial:  https://www.atlassian.com/git/tutorials
* **Valentin Jaimes-Costilla**  
Used first Sprint to research Javascript Language and Git functionality to be better able to participate and make updates to project file and stay up to date with the current state of the project.
Javascript Tutorial: https://www.youtube.com/watch?v=hdI2bqOjy3c
Git Tutorial: https://www.youtube.com/watch?v=8JJ101D3knE

## What is it?
Our team is creating a more visually creative and engaging typing test website that will encourage
the user by giving active feedback on their word count progress.

## Who is this for?
Our audience is anyone who wants to improve their typing speed, but would also like a boost of motivation
to return to the website. This is what we hope to achieve with a more interactive experience.	

## Why are we doing this?
Typing test websites as they are now are useful for anyone who wants to get a measure of how fast they can
type. However, the majority of these websites are plain and don't give much encouragement to the user to
improve or come back to the site. We hope to change with a more engaging website that will excite the user
and strengthen their resolve to improve their typing speed.

## Technologies Used
- [HTML](https://developer.mozilla.org/en-US/docs/Web/HTML)
- [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS)
- [Javascript](https://www.javascript.com/)

## Features
* ## Real time stats
    * Record and presents an up-to-date speed and accuracy tracker for any user, 
        especially users that wish to keep track of their progress over time.
    * I, Peter, want to see the speed and accuracy I have when typing in real world
        time so that  it could make typing more enjoyable and improve my typing 
        form
* ## Always Updating Dictionaries
    * New vocabulary (possibly themed) is always being added, to keep users 
        engaged.
    * I, Lucas Grogean, as a typer, want to add a new vocabulary of words with 
        new backgrounds to keep my focus engaged.
* ## Custom Dictionaries
    * The user will be able to add their own words/ phrases to a library and then 
        complete challenges using their own library.
* ## Various Metrics
    * We’ll use other metrics than wpm and accuracy to give the user as much 
        feedback as possible.
    * I, Lucas Grogean, as a learner want to a new way of calculating WPM so that I
        can get a good estimate of how fast I type in multiple ways.
* ## Arcade Mode
    * There will be a few games that can be played, this will make typing more fun 
        for people that don’t always want to practice in “traditional mode”.
    * As an Texas State University Student, I would like the typing website to have 
        different game modes for typing.
* ## Color feedback system
    * As the user reaches some words per minute goal (set by the user or by the 
        system) then some words may begin to change color to indicate distance 
        from the goal.
* ## Hard Mode
    * Makes typing box transparent, reverses words from dictionary


## Acknowledgements
This project is inspired and based on [typings.gg](https://github.com/briano1905/typings#typings), a great starting point for a great typing game. 

Hard mode snake game: [Basic Snake HTML Game](https://gist.github.com/straker/ff00b4b49669ad3dec890306d348adc4)

