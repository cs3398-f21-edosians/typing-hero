function login(user, wpmRecord) {
    localStorage.setItem("Username", user);
    localStorage.setItem("Hi-Score", wpmRecord);
    location.replace("index.html")
}

function guestLogin() {
  localStorage.setItem("Username", "Guest");
  localStorage.setItem("Hi-Score", 0);
  location.replace("index.html")
}

function loadData() {
    var url="https://api.apispreadsheets.com/data/20605/";
    xmlhttp=new XMLHttpRequest();
    var return_data;
    let user = "\"" + document.getElementById("user").value + "\"";
    let pass = "\"" + document.getElementById("pass").value + "\"";
    let maxWPM = 0;
    
    let current_entry = "";
    let found = false;

    xmlhttp.onreadystatechange = function() {
      if(xmlhttp.readyState == 4 && xmlhttp.status==200){
        return_data = xmlhttp.responseText;

        for (let i = 0; i < return_data.length; i++) {
          current_entry += return_data.charAt(i);

          if (return_data.charAt(i) == "}") {
            if(current_entry.includes(user) && current_entry.includes(pass)) {
              found = true;
              i = return_data.length;
              wpmRecord = return_data.current_entry(message);
            }
            else {
              current_entry = "";
            }
          }
        }

        if (found == true) {
          login(user, wpmRecord);
          document.getElementById("current_user").innerHTML("test1");
        }
        else {
          window.alert("Incorrect Username or Password");
        }
      }
    };
    
    xmlhttp.open("GET",url,true);
    xmlhttp.send(null);
}

function register() {
    document.getElementById('id02').action = "index.html";
}

function SubForm() {
    $.ajax({
      url:"https://api.apispreadsheets.com/data/20605/",
      type:"post",
      data:$("#id02").serializeArray(),
    });

    $(document).ready( function() { alert("Successfully Registered"); });
    register();
}

function updateWPM() {
  
}